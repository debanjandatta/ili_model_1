import os
import sys

sys.path.append ('./..')
sys.path.append ('./../..')
import pandas as pd
import numpy as np
import ili_datapipeline.DataFrame.data_extractor as data_extractor
import ili_datapipeline.common_utils as data_common_utils
import ili_datapipeline
import config


# data_obj will be training and test data holders
class data_obj:
    def __init__(self, config_obj, start_year, start_week, end_year, end_week):
        self.extractor_obj = data_extractor.extarctor ()
        # set up the data frames
        self.config_obj = config_obj
        self.start_year = start_year
        self.start_week = start_week
        self.end_year = end_year
        self.end_week = end_week
        self.region = config_obj.region
        self.data_keys = []
        self.df_attribute = {}
        self.df_dict = {}
        self._setup_cdc_ili_data ()
        self._setup_cdc_strain_data ()
        self._setup_gst_data ()
        self._setup_holidays_data ()
        self._setup_weather_data ()


    def _setup_weather_data(self):

        self.feature_stat_map = self.config_obj.feature_stat_map
        self.weather_data = {}
        for feature, stat_list in self.feature_stat_map.iteritems ():
            for stat in stat_list:
                _df = self.extractor_obj.get_weather_data (self.start_year, self.start_week, self.end_year,
                                                           self.end_week,
                                                           self.region, feature, stat)
                key = 'weather'+ '-' + feature + '-' + stat
                self.df_attribute[key] = 'value'
                self.data_keys.append(key)
                self.df_dict[key] = _df

        return

    def _setup_cdc_ili_data(self):
        _df = self.extractor_obj.get_cdc_ili_data (self.start_year, self.start_week, self.end_year,
                                                                 self.end_week, self.region)
        key = 'ili'
        self.data_keys.append(key)
        self.df_attribute[key] = 'wili'
        self.df_dict[key] = _df
        return

    def _setup_cdc_strain_data(self):
        _df = self.extractor_obj.get_cdc_strain_data (self.start_year, self.start_week, self.end_year,
                                                                       self.end_week, self.region)
        key = 'strain'
        self.data_keys.append(key)
        self.df_attribute[key] = 'pc_A'
        self.df_dict[key] = _df
        return

    def _setup_gst_data(self):
        _df = self.extractor_obj.get_gst_data (self.start_year, self.start_week, self.end_year, self.end_week,
                                                         self.region)
        key = 'gst'
        self.data_keys.append(key)
        self.df_attribute[key] = 'value'
        self.df_dict[key] = _df
        return

    def _setup_holidays_data(self):
        _df = self.extractor_obj.get_holidays_data (self.start_year, self.start_week, self.end_year,
                                                                   self.end_week)
        key = 'holidays'
        self.data_keys.append(key)
        self.df_attribute[key] = 'holiday'
        self.df_dict[key] = _df
        return

    def get_all_keys(self):
        return self.data_keys


def get_X():
    start_year = 2003
    start_week = 21
    end_year = 2017
    end_week = 20
    region  = 'nat'
    return data_obj(start_year,start_week,end_year,end_week,region)

def get_Y():
    start_year = 2016
    start_week = 21
    end_year = 2017
    end_week = 20
    region = 'nat'
    return data_obj (start_year, start_week, end_year, end_week, region)

def get_data_obj(config_obj, start_year,start_week,end_year,end_week,lookback = 0):
    if start_week - lookback < 1 :
        start_week = data_common_utils.num_weeks_in_year (start_year-1) + (start_week - lookback)
        start_year = start_year -1
    else :
        start_week = start_week - lookback

    return data_obj(config_obj, start_year,start_week,end_year,end_week)
