import pandas as pd
import numpy as np
import data_feeder
import config
import frechett
import sys
from sklearn import linear_model
import cPickle
from joblib import Parallel, delayed
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from pyearth import Earth

sys.path.append ('./..')
import ili_datapipeline.common_utils as data_common_utils
import time
import sklearn.metrics


class model:
    def __init__(self, config_obj):
        self.window_map = config_obj.window_map
        self.lag_map = config_obj.lag_map
        self.max_jobs = 1
        self.region = config_obj.region
        self.config_obj = config_obj
        # x is weeks
        # y1 is predicted
        # y2 is actual
        self.experiment_dir = config_obj.experiment_dir
        self.figure_output_dir = config_obj.figure_output_dir

        # hyperparamters
        self.max_lookback = (max (self.window_map.values ()) + max (self.lag_map.values ()))
        return

    def get_week_list(self, year):
        num_weeks = data_common_utils.num_weeks_in_year (year)
        week_list = range (self.config_obj.season_start_week, num_weeks + 1)
        week_list.extend (range (1, self.config_obj.season_end_week + 1))
        return week_list

    # ------ #
    # Calculate similarity

    # Use frechett distance1!
    @staticmethod
    def sim_score(list_1, list_2):
        return frechett.frechetDist (list_1, list_2)

    @staticmethod
    def _get_base(df, year, week, column_name, window, lag):
        try:
            _idx = df.loc[(df['year'] == year) & (df['week'] == week)].index[0]

            if _idx - lag < 0:
                print 'Error !!  check lag setting!!', df.columns, column_name, window, lag
                _idx = max (_idx - lag, 0)

            ub = _idx
            lb = _idx - window + 1
            if lb < 0:
                print 'Error !!  check window setting!!', df.columns, column_name, window, lag
                lb = max (_idx - window + 1, 0)  # Ensure no negative index

            _list_vals = list ((df.loc[range (lb, ub + 1)])[column_name])
            return _list_vals

        except:
            print df
            print year
            print week
            print column_name
            exit (1)

    # input  :
    # src_df : who's values at a time point are taken to be searched
    # search_df : in which the values are searched
    # 2 modes , train and test
    # return the index with minimum score
    @staticmethod
    def get_closest(src_df, search_df, search_indices, window, lag, src_year, src_week, column_name):
        _list_vals = model._get_base (src_df, src_year, src_week, column_name, window, lag)

        min_score = sys.maxint  # Sentinel value
        min_score_idx = None

        for index in search_indices:
            cur_seach_range = range (index - window + 1, index + 1)
            cur_list = list ((search_df.loc[cur_seach_range])[column_name])
            try:
                score = model.sim_score (cur_list, _list_vals)
            except:
                print cur_list
                print src_df
                print src_year, src_week, column_name
                print search_indices
                exit (1)
            if score < min_score:
                min_score = score
                min_score_idx = index

        yr, wk = search_df.loc[min_score_idx]['year'], search_df.loc[min_score_idx]['week']
        return int (yr), int (wk)

    def extract_base_and_search(self, df, year):
        max_lookback = self.max_lookback
        all_years = list (df.year.unique ())
        all_years.remove (max (all_years))
        all_years.remove (year)

        removal_idx_start = df.loc[(df['year'] == year) & (df['week'] == self.config_obj.season_start_week)].index[0]
        removal_idx_end = df.loc[(df['year'] == year + 1) & (df['week'] == self.config_obj.season_end_week)].index[0]
        src_df = df.loc[(removal_idx_start - max_lookback):removal_idx_end + 1]
        search_df = pd.DataFrame (df)

        # Create the search df
        # remove indices : [y,20-max_lookback : y+1,21]
        _rem_start = removal_idx_start
        _rem_end = removal_idx_end - max_lookback
        _rem_range = range (_rem_start, _rem_end + 1)
        search_df = search_df.drop (search_df.index[_rem_range])

        try:
            search_df = search_df.reset_index ()
            del search_df['index']
        except:
            pass

        search_indices = []
        # search_indices should be from each years week 21 to next year's week 20
        for y in all_years:
            idx = \
            search_df.loc[(search_df['year'] == y) & (search_df['week'] == self.config_obj.season_start_week)].index[0]
            _idxs = range (idx, idx + data_common_utils.num_weeks_in_year (y))
            search_indices.extend (_idxs)
        return src_df, search_df, search_indices


    # define a local function to process the innards of loop so that we can parallelize

    def _training_aux(self,data_key, data_obj_train, week_list, _train_year, ili_df, train_year):
        global window_map, lag_map

        attribute = data_obj_train.df_attribute[data_key]
        df = data_obj_train.df_dict[data_key]
        # separate base and search list
        src_df, search_df, search_indices = self.extract_base_and_search (df, train_year)

        try:
            window_val = window_map[data_key]
        except:
            window_val = self.config_obj.defualt_window
        try:
            lag_val = lag_map[data_key]
        except:
            lag_val = 0

        potential_ili = []
        for week, t_yr in zip (week_list, _train_year):
            _y, _w = self.get_closest (src_df=src_df,
                                  search_df=search_df,
                                  search_indices=search_indices,
                                  window=window_val,
                                  lag=lag_val,
                                  src_year=t_yr,
                                  src_week=week,
                                  column_name=attribute
                                  )
            _y = int (_y)
            _w = int (_w)

            idx = ili_df.loc[(ili_df['year'] == _y) & (ili_df['week'] == _w)].index[0]
            _ili_sim = ili_df.loc[idx]['wili']
            potential_ili.append (_ili_sim)

        return [data_key, potential_ili]


    def train(self):


        train_year_start = 2013
        train_year_end = 2015
        train_years = range (2013, 2015 + 1)

        data_obj_train = data_feeder.get_data_obj (self.config_obj,
                                                   train_year_start,
                                                   self.config_obj.season_start_week,
                                                   train_year_end + 1,
                                                   self.config_obj.season_end_week,
                                                   self.max_lookback)
        data_keys = data_obj_train.get_all_keys ()
        data_keys.remove ('ili')

        X = {}
        Y = []

        for d in data_keys:
            X[d] = []

        for train_year in train_years:

            week_list = self.get_week_list (train_year)
            ili_df = data_obj_train.df_dict['ili']
            train_year_arr = [train_year] * (data_common_utils.num_weeks_in_year (train_year) - 21 + 1)
            train_year_arr.extend ([train_year + 1] * 20)
            # results = Parallel (n_jobs=10) (delayed (self._training_aux) (data_key,
            #                                                          data_obj_train,
            #                                                          week_list,
            #                                                          train_year_arr,
            #                                                          ili_df,
            #                                                          train_year)
            #                                 for data_key in data_keys)
            results = []
            for data_key in data_keys:

                res = self._training_aux (data_key,
                data_obj_train,
                week_list,
                train_year_arr,
                ili_df, train_year)
                results.append (res)


            for r in results:
                X[r[0]].extend (r[1])

        for data_key in data_keys:
            X[data_key] = np.asarray (X[data_key])
        _X = np.asarray (X.values ())
        X = _X
        X = np.transpose (X, [1, 0])

        for train_year in train_years:
            ili_df = data_obj_train.df_dict['ili']
            _train_year = [train_year] * (data_common_utils.num_weeks_in_year (train_year) - 21 + 1)
            _train_year.extend ([train_year + 1] * 20)
            week_list = self.get_week_list (train_year)
            for week, t_yr in zip (week_list, _train_year):
                _real_ili_idx = ili_df.loc[(ili_df['year'] == t_yr) & (ili_df['week'] == week)].index[0]
                _real_ili = ili_df.loc[_real_ili_idx]['wili']
                Y.append (_real_ili)

        Y = np.asarray (Y)
        # reg = linear_model.Ridge (alpha=.15)
        self.regressor = Earth ()
        self.regressor.fit (X, Y)


    def test(self):

        region = self.config_obj.region
        window_map = self.config_obj.window_map
        lag_map = self.config_obj.lag_map
        test_year = 2016

        num_weeks = data_common_utils.num_weeks_in_year (test_year)

        data_obj_test = data_feeder.get_data_obj (self.config_obj,
                                                  2013,
                                                  self.config_obj.season_start_week,
                                                  test_year + 1,
                                                  self.config_obj.season_end_week,
                                                  self.max_lookback)
        data_keys = data_obj_test.get_all_keys ()
        data_keys.remove ('ili')
        X = {}
        Y = []

        for d in data_keys:
            X[d] = []

        ili_df = data_obj_test.df_dict['ili']
        week_list = self.get_week_list (test_year)
        test_year_arr = [test_year] * (data_common_utils.num_weeks_in_year (test_year) - 21 + 1)
        test_year_arr.extend ([test_year + 1] * 20)

        for data_key in data_keys:

            attribute = data_obj_test.df_attribute[data_key]
            df = data_obj_test.df_dict[data_key]
            potential_ili = []

            src_df, search_df, search_indices = self.extract_base_and_search (df, test_year)
            try:
                window_val = window_map[data_key]
            except:
                window_val = self.config_obj.defualt_window
            try:
                lag_val = lag_map[data_key]
            except:
                lag_val = 0

            for week, t_yr in zip (week_list, test_year_arr):
                _y, _w = self.get_closest (src_df=src_df,
                                      search_df=search_df,
                                      search_indices=search_indices,
                                      window=window_val,
                                      lag=lag_val,
                                      src_year=t_yr,
                                      src_week=week,
                                      column_name=attribute)
                _y = int (_y)
                _w = int (_w)

                idx = ili_df.loc[(ili_df['year'] == _y) & (ili_df['week'] == _w)].index[0]
                _ili_sim = ili_df.loc[idx]['wili']
                potential_ili.append (_ili_sim)
            X[data_key].extend (potential_ili)

        for data_key in data_keys:
            X[data_key] = np.asarray (X[data_key])
        _X = np.asarray (X.values ())
        X = _X
        X = np.transpose (X, [1, 0])

        ili_df = data_obj_test.df_dict['ili']
        week_list = self.get_week_list (test_year)
        for week, t_yr in zip (week_list, test_year_arr):
            _real_ili_idx = ili_df.loc[(ili_df['year'] == t_yr) & (ili_df['week'] == week)].index[0]
            _real_ili = ili_df.loc[_real_ili_idx]['wili']
            Y.append (_real_ili)
        Y = np.asarray (Y)

        result = self.regressor.predict (X)
        weeks = range (1, num_weeks + 1)

        error = sklearn.metrics.mean_squared_error (Y, result)
        # plotter (weeks, result, Y, file_name)

        true_val = Y
        predicted = result
        return error, predicted, true_val


    def execute(self):
        self.train ()
        error, predicted, true_val = self.test ()
        return error, predicted, true_val







# config_obj = config.config()
# model_obj = model(config_obj)
# err,pred,true_val = model_obj.execute()
# x= range(len(true_val))
# plotter(x, pred, true_val,'bal.png')
