import config
import math
from itertools import compress
import pprint as pp
from multiprocessing import Process
import multiprocessing as mp
import model
import os
from collections import OrderedDict
import cPickle as pickle
import pprint

# ------------------------------ #
# def setup_dirs():
#     cwd = os.getcwd ()
#     dir = config_file.experiment_dir
#     if not os.path.exists (dir):
#         os.makedirs (dir)
#     os.chdir (dir)
#     dir = config_file.figure_output_dir
#     if not os.path.exists (dir):
#         os.makedirs (dir)
#     os.chdir (cwd)
#
# setup_dirs ()
# ------------------------------ #

max_jobs = 100
min_lag = 1
max_lag = 4
min_window = 1
max_window = 5
config_id_error_map = {}
config_id_error_map_file = 'config_id_error.txt'
config_id_config_file = 'config_id_config.txt'



# ------------------------------ #
# input :
# config_id int
# config :
# feature_stat_map, window_map, lag_map
# ------------------------------ #

op_dir_loc = 'result_configs'

def write_result(config_id, error, pred_value,real_value):
    global op_dir_loc
    print config_id, error, pred_value,real_value
    cwd = os.getcwd()

    if not os.path.exists (op_dir_loc):
        os.makedirs (op_dir_loc)

    os.chdir(op_dir_loc)
    f_name = str(config_id)+'.p'
    store_obj = {}
    store_obj['id'] = config_id
    store_obj['error'] = error
    store_obj['pred'] = pred_value
    store_obj['real'] = real_value
    pickle.dump (store_obj, open (f_name, "wb"))
    os.chdir(cwd)
    return


def _config_executor(config_id, config_dict):
    config_obj = config.config ()
    config_obj.feature_stat_map = config_dict[0]
    config_obj.window_map = config_dict[1]
    config_obj.lag_map = config_dict[2]

    model_obj = model.model (config_obj)
    error , pred_val,real_value = model_obj.execute ()
    print ' >> Config id ', config_id , 'Error ', error
    try:
        write_result(config_id, error, pred_val,real_value)
    finally:
        pass
    return (config_id, error, config_dict, pred_val,real_value)


def exceute_and_record():
    global max_jobs
    global config_id_error_map_file
    global config_id_config_file
    config_dict = get_configs ()
    pool = mp.Pool (processes=max_jobs)
    # config_dict = {k: config_dict[k] for k in config_dict.keys ()[:5]}
    results = [pool.apply_async (_config_executor, args=(config_id, config)) for config_id, config in config_dict.iteritems ()]
    output = [p.get () for p in results]

    config_id_error_map = {}
    config_id_config_map = {}
    for op in output :
        config_id = op[0]
        err = op[1]
        config_dict = op[2]
        config_id_error_map[config_id] = err
        config_id_config_map[config_id] = config_dict


    # sort the errors!
    config_id_error_map = OrderedDict (sorted (config_id_error_map.items (), key=lambda x: x[1]))
    # Write config_id_error_map and config_dict to file
    logFile = open (config_id_error_map_file, 'w')
    pp = pprint.PrettyPrinter (indent=4, stream=logFile)
    pp.pprint (config_id_error_map)
    logFile = open (config_id_config_file, 'w')
    pp = pprint.PrettyPrinter (indent=4, stream=logFile)
    pp.pprint (config_id_config_map)
    return

    # config_id_error_map = {}
    # config_id_pred_map = {}
    # config_id_real_map = {}
    # # Write to a dictionary !
    #
    # for r in results:
    #     # break down each tuple!
    #     config_id = r[0]
    #     error = r[1]
    #     pred_vals = r[2]
    #     real_vals = r[3]
    #     config_id_error_map[config_id] = error
    #     config_id_pred_map[config_id] = pred_vals
    #     config_id_real_map[config_id] = real_vals
    #
    # # sort the errors!
    # config_id_error_map = OrderedDict (sorted (config_id_error_map.items (), key=lambda x: x[1]))
    # # Write config_id_error_map and config_dict to file
    # pp.pprint (config_id_error_map, config_id_error_map_file)
    # pp.pprint (config_dict, config_id_config_file)
    #
    # # Record everything 3 pickle objects !
    # pickle.dump (config_id_error_map, open ("config_id_error_map.p", "wb"))
    # pickle.dump (config_dict, open ("config_dict.p", "wb"))
    # pickle.dump (config_id_pred_map, open ("config_id_pred_map.p", "wb"))
    # pickle.dump (config_id_real_map, open ("config_id_real_map.p", "wb"))
    # return


def boolean_index(n, num_indices):
    _res = list (str (bin (n)[2:]).zfill (num_indices))
    _res = [bool (int (x)) for x in _res]
    return _res


def _add_in_lags(_map):
    _map['strain'] = 1
    _map['gst'] = 1
    _map['holidays'] = 1
    return _map


def _add_in_window(_map):
    _map['strain'] = 3
    _map['gst'] = 3
    _map['holidays'] = 2
    return _map


def get_configs():
    config_dict = {}
    global min_window, max_window, min_lag, max_lag
    stats = config.stats
    features = config.features
    num_f = len (features)
    num_s = len (stats)
    config_id = 1
    for i in range (int (math.pow (2, num_f)) + 1):
        _f_indices = boolean_index (i, num_f)
        cur_features = list (compress (features, _f_indices))

        if len (cur_features) == 0:
            continue

        # create the map!
        w_map = {}

        for j in range (int (math.pow (2, num_s)) + 1):
            for cf in cur_features:
                w_map[cf] = []
            _s_indices = boolean_index (j, num_s)
            cur_stats = list (compress (stats, _s_indices))

            if len (cur_stats) == 0:
                continue

            for cf in cur_features:
                w_map[cf].extend (cur_stats)

            # pp.pprint(w_map)
            # print ' config_id > ', config_id
            # use window value, lag value for each

            for window_val in range (min_window, max_window + 1):
                for lag_val in range (min_lag, max_lag + 1):
                    _window_map = {}
                    _lag_map = {}
                    for cf1 in cur_features:
                        for cs1 in cur_stats:
                            key = '-'.join (['weather', cf1, cs1])
                            _lag_map[key] = lag_val
                            _window_map[key] = window_val
                            # Add in strain, gst , and holiday lags & windows
                            _lag_map = _add_in_lags (_lag_map)
                            _window_map = _add_in_window (_window_map)
                            config_dict[config_id] = (w_map, _window_map, _lag_map)
                            config_id += 1

    return config_dict


exceute_and_record ()
