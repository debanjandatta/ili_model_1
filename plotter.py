import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def plot(x, y1, y2, file_name=None):
    show = False
    file_path = None

    if file_name is not None:
        # file_path = self.experiment_dir + '/' + self.config_obj.figure_output_dir + '/' + file_name
        file_path = file_name
    else:
        show = True

    fig = plt.figure (figsize=(10, 10))
    red = mpatches.Patch (color='red', label='Predicted ILI values')
    blue = mpatches.Patch (color='blue', label='Real ILI values')
    plt.plot (x, y1, 'ro-', x, y2, 'bo-', label='...')
    plt.legend (handles=[red, blue])

    try:
        if show:
            plt.show ()
        else:
            fig.savefig (file_path)
    except:
        print(' Error generating image !')

    return