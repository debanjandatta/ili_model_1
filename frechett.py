# Reference :
# http://www.kr.tuwien.ac.at/staff/eiter/et-archive/cdtr9464.pdf

import numpy as np
import math
import numpy.linalg as linalg


def euc_dist(p1,p2):
    return math.fabs(p1-p2)
    # return linalg.norm(np.asarray(p1) - np.asarray(p2),ord=2)

def calc(ca,i,j,P,Q):

    if ca[i,j] > -1:
        return ca[i,j]
    elif i == 0 and j == 0:
        ca[i,j] = euc_dist(P[0],Q[0])
    elif i > 0 and j == 0:
        ca[i,j] = max(calc(ca,i-1,0,P,Q),euc_dist(P[i],Q[0]))
    elif i == 0 and j > 0:
        ca[i,j] = max(calc(ca,0,j-1,P,Q),euc_dist(P[0],Q[j]))
    elif i > 0 and j > 0:
        ca[i,j] = max(min(calc(ca,i-1,j,P,Q),calc(ca,i-1,j-1,P,Q),calc(ca,i,j-1,P,Q)),euc_dist(P[i],Q[j]))
    else:
        ca[i,j] = float("inf")
    return ca[i,j]


def frechetDist(P,Q):
    ca = np.ones((len(P),len(Q)))
    ca = np.multiply(ca,-1)
    return calc(ca,len(P)-1,len(Q)-1,P,Q)