from multiprocessing import Pool
from multiprocessing import Process
from multiprocessing import Process, Queue
import matplotlib.pyplot as plt
import random
import numpy as np
import matplotlib.patches as mpatches



def plotter(file_name=None):
    fig = plt.figure (figsize=(3,3))
    fig.canvas.set_window_title ('some crap')
    ax = fig.add_subplot (111)
    red = mpatches.Patch (color='red', label='Predicted ILI values')
    blue = mpatches.Patch (color='blue', label='Real ILI values')
    x = range(1,10+1)
    y = np.random.randn(10)*random.random()+random.random()
    ax.plot (x, y, 'ro-', label='...')
    ax.legend (handles=[red, blue])
    if file_name is None:
        plt.show ()
    else:
        file_path = file_name
        fig.savefig (file_path)
    return


_m = { }
for i in range(10):
    x = random.randint(0,(i+2)*10+5)
    y = x*x
    _m[x] = y



def f1(arg1):
    print arg1[0],arg1[1]
    fn = str(arg1[0])+'.png'
    plotter(fn)
    return (arg1[0],arg1[1]-10)

p = Pool(10)
z =_m.items()
res = (p.map(f1,z))

print res