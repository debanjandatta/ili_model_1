features = ['abs_hum', 'rel_hum', 'sp_hum', 'temp', 'v_wind', 'u_wind']
stats = ['week_median',
         'week_max',
         'week_min',
         'week_variance',
         'week_max_daily_variation',
         'week_min_daily_mean']


class config:
    def __init__(self):
        self.stats = [
            'week_median',
            'week_max',
            'week_min',
            'week_variance',
            'week_max_daily_variation',
            'week_min_daily_mean']

        self.features = ['abs_hum', 'rel_hum', 'sp_hum', 'temp', 'v_wind', 'u_wind']

        self.feature_stat_map = {
            'abs_hum': ['week_median'],
            'rel_hum': ['week_median'],
            'sp_hum': ['week_median'],
            'temp': ['week_median'],
            'u_wind': ['week_median'],
            'v_wind': ['week_median']
        }

        self.window_map = {
            'ili': 1,
            'strain': 3,
            'gst': 1,
            'holidays': 2,
            'weather-temp-week_median': 4,
            'weather-abs_hum-week_median': 4,
            'weather-rel_hum-week_median': 4,
            'weather-abs_hum-week_max_daily_variation': 1
        }

        self.lag_map = {
            'ili': 0,
            'strain': 1,
            'gst': 1,
            'holidays': 1,
            'weather-temp-week_median': 1,
            'weather-abs_hum-week_median': 1,
            'weather-abs_hum-week_max_daily_variation': 1
        }

        self.region = 'hhs6'
        self.season_start_week = 21
        self.season_end_week = 20
        self.defualt_window = 4
        self.experiment_dir = '1'
        self.figure_output_dir = 'plots'

        return
